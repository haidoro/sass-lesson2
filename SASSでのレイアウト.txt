
SASSを使ったページレイアウト

ファイル構成
index.htmlとstyle.cssは同階層にあるものとします。
リセットCSSはbaseフォルダ内のlibフォルダ内にあります。
baseフォルダには_reset.scssと_variables.scssファイルがあります。
_reset.scssはリセットCSSをインポートするためのファイルです。
_variables.scssは変数を宣言するためのファイルです。
このフォルダの直下にあるstyle.scssはそれぞれのscssファイルをとりまとめるファイルとなります。

このように複雑なファイル構成にした理由は拡張性を向上させるためです。

*************sass_sample1******************
リセットCSSのインポートと変数の宣言

１，sanitize.cssを下記URLからダウンロード
https://jonathantneal.github.io/sanitize.css/

２，ダウンロードファイル名を「 _sanitize.scss」にしてlibフォルダに保存。

３，「_reset.scss」ファイルを作成して２をインポートする。
@import "./lib/sanitize";

４，「style.scss」を作成して３をインポートする。
@import "./base/reset";

５，コアラでコンパイルするとstyle.cssができる。
style.cssの内容は_sanitize.scssのものと同じになっている。

変数宣言ファイル作成
変数は $マークで始まる変数名で作成してbaseフォルダに格納します。
ファイル名は_variables.scssとします。

配色計画の色を変数に格納し、必要に応じて呼び出します。

$primary-color:#07075F;
$secondary-color:#151596;
$accent-color:#89739F;
$background-color:#07155F;
$color-main-text:#474747;
$color-link:#878378;

変数のインポートはresetより前、先頭で行います。
@import "./base/variables";

*************sass_sample2******************
コンポーネントのデザインはcomponentsフォルダ内のファイルでコンポーネントごとにファイルを分けて管理します。そうすることで、別のサイトでの使いまわしが楽になります。

header部分の作成
header部分は.Headerで構築されています。
header内は.Header__headにh1があり、.Header__bodyはナビゲーションになっています。.Header__headと.Header__bodyは横並びになっていますが、今回はFlexBoxを使用してレイアウトしています。今後FlexBoxを使用したヘッダーのレイアウトでは使い回しができます。

.Header {
    display:flex;
    justfy-content:flex-start;
    align-item:center;
    min-height:98px;
	&__head{
		flex:1 1 auto;
		text-align: center;
	}
	&__body{
		flex:3 1 auto;
	}
}

メニュー部分のレイアウトも別ファイルとしてコンポーネント化します。
.Menu{
	display:flex;
	justify-content:space-around;
	&__item{
		display:block;
		padding:10px 15px;
		text-transform: uppercase;
	}
}

style.scssでインポートします。
@import "./components/header";
@import "./components/menu";

*************sass_sample3******************

画像＋記事のコンポーネントを作成
 .EntryPanel
 .EntryPanel__sub            .EntryPanel__main
  ________________           .EntryPanel__head
 |                         |           　　　見出し
 |                         |　　　 .EntryPanel__body
 |                         |
 |                         |              　　 内容
 |                         |            .EntryPanel__foot
 |_______________ |                     ボタン
 .EntryPanel__thumb


.EntryPanel部分の作成
entryPanel.scssファイルを作成してstyle.scssでインポートします。
レイアウトに使用するプロパティはflexを使用します。

.EntryPanel{
	display:flex;
	align-items:center;
	&__main{
		display:flex;
		justify-content:space-between;
		align-self:stretch;
		flex:1 1 55%;
		flex-direction:column;
		justify-content:space-between;
		align-self:stretch;
		padding:20px 0;
	}
	&__sub{
		flex:1 1 45%;
		margin-right:20px;
	}
	&__thumb{
		width:100%;
		height:auto;
		position:relative;
		flex:1 1 45%;
		// padding-top: 40.5%;
		overflow: hidden;
		margin-right:$space-unit;
	}
	&__img{
		position:absolute;
		top:50%;
		left:50%;
		// transform:translate(-50%, -50%);
		margin-left:-50%;
		margin-top:-50%;
		width:auto;
		height:auto;
		max-width: 150%;
		max-height: 150%;
		min-width: 100%;
		min-height:100%;
	}
	&__head{
		font-weight:bold;
		font-size:16px;
	}
	&__body{
		margin-top:10px;
		font-size: 14px;
	}
	&__foot{
		margin-top:10px;
		text-align: right;
	}
}

基本になるレイアウトも別ファイルにします。
layouts.scss
.l-column{
	flex:0 1 $base-content-width;
	max-width: $base-content-width;
	margin:0 auto;
	padding:0 $space-unit;
}
.l-row{
	display:flex;
	justify-content:space-between;
	flex-wrap:wrap;
	margin:0 auto;
}
.l-halfColumn{
	flex:1 0 0;
	max-width: ($base-content-width - $column-space)/2;
	margin-right:$column-space;
	&:nth-child(even){
		margin-right:0;
	}
}
.l-halfTile{
	display:flex;
	flex:1 1 50%;
	margin:0;
	padding:0;
}
.l-quaterColumn{
	flex:1 0 0px;
	max-width:($base-content-width - $column-space*3)/4;
	margin-right: $column-space;
	&:nth-child(4n){
		margin-right: 0;
	}
}